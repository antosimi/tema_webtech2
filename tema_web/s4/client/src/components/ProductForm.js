import React, { Component } from 'react'

class ProductForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      productName : '',
      price : -1,
    }
    this.handleChange = (evt) => {
      this.setState({
        [evt.target.name] : evt.target.value
      })
    }
  }
  render() {
    return (
      <div>
        <form>
          <label for="productName">productName </label>
          <input type="text" id="productName" name="productName" onChange={this.handleChange} />
          <label for="price">price </label>
          <input type="text" id="price" name="price" onChange={this.handleChange} />

          <input type="button" value="add" onClick={() => this.props.onAdd({
            productName : this.state.productName,
            price : this.state.price,

          })} />
        </form>
      </div>
    )
  }
}

export default ProductForm
