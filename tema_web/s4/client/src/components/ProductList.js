import React, { Component } from 'react'
import ProductStore from './ProductStore'
import Product from './Product'
import ProductForm from './ProductForm'

class ProductList extends Component {
  constructor(){
    super()
    this.store = new ProductStore()
    this.state = {
      products : [],
      selectedProduct : null
    }
    this.add = (product) => {
      this.store.addOne(product)
    }
    this.save = (id, student) => {
      this.store.saveOne(id, student)
    }
  }
  componentDidMount(){
    this.store.getAll()
    this.store.emitter.addListener('GET_ALL_SUCCESS', () => {
      this.setState({
        students : this.store.content
      })
    })
  }
  render() {
      return (
        <div>
          {
            this.state.products.map((e, i) => <Product item={e} key={i} onSave={this.save} />)
          }
          <ProductForm onAdd={this.add} />
        </div>
      )
    }

  }


export default ProductList
