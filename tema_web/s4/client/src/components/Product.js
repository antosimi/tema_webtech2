import React, { Component } from 'react'

class Product extends Component {
  constructor(props){
    super(props)
    this.state = {
      productName : this.props.item.productName,
      price : this.props.item.price
    }
    this.handleChange = (evt) => {
      this.setState({
        [evt.target.name] : evt.target.value
      })
    }
  }
  render() {
    let {item} = this.props
      return (
        <div>
          Product {item.productName} costs {item.price} 
        </div>
      )
    
  }
}

export default Product