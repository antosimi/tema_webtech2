const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/:pid', (req,res) => {
    if(req.body.productName && req.body.price){
        let pid = req.params.pid
        products.find(x => x.id == pid).productName = req.body.productName
        products.find(x => x.id == pid).price = req.body.price
        res.status(200).send("Edit successful")
    }
    else{
        res.status(500).send("Error!")
    }
})

app.delete("/del", (req, res) => {
    if(req.body.productName){
        let name = req.body.productName
        let index = products.findIndex(x => x.productName == name)
        delete(products[index])
        res.status(200).send("Delete successful")
    }
    else{
        res.status(500).send("Error!")
    }
})

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});